import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClient } from '@angular/common/http';

import { UtilServicio } from '././servicio/util.servicio';
import { ApiServicio } from '././servicio/api.servicio';
import { MessageDialog } from './componente/otro/message.dialog';
import { TipoDocumento } from './componente/principal/tipo.documento'

import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
// import {  } from 'primeng/';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextModule } from 'primeng/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { MessageService } from 'primeng/api';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';

// import {
//   SharedModule, , , CheckboxModule, MultiSelectModule, CalendarModule,
//   TabViewModule, DropdownModule, PanelModule, SplitButtonModule, PanelMenuModule, ToolbarModule, ConfirmDialogModule, ConfirmationService, DialogModule, OverlayPanelModule, DataListModule,
//   TreeModule, InputSwitchModule
// } from 'primeng';

@NgModule({
  declarations: [
    AppComponent,
    TipoDocumento
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    CheckboxModule,
    FieldsetModule,
    InputTextModule,
    MenubarModule,
    TableModule,
    TabViewModule,
    ToastModule
    
  ],

  providers: [
    HttpClient,

    ApiServicio,
    UtilServicio,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
