import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { TipoDocumentoEntidad } from '../../entidad/tipo.documento';

import { MessageDialog } from '../otro/message.dialog';
import { UtilServicio } from '../../servicio/util.servicio';
import { ApiServicio } from '../../servicio/api.servicio';

import { MessageService } from 'primeng/api';

import { InputTextModule } from 'primeng/inputtext';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';

@Component({
    selector: 'tipo-documento',
    templateUrl: '../../plantilla/principal/tipo.documento.html'
})
export class TipoDocumento implements OnInit{

    listaTipoDocumento: TipoDocumentoEntidad[];

    // constructor(private apiServicio: ApiServicio, private utilServicio: UtilServicio) {
    constructor(private messageService: MessageService) {
        this.listaTipoDocumento = [];
    }

    ngOnInit(){
        this.obtenerListaTipoDocumento();
    }

    guardar() {
        this.mostrarMensaje("Se guardó correctamente");
    }

    mostrarMensaje(mensaje: string) {
        this.messageService.add({
            severity:'success',
            summary:mensaje,
            detail:'Via MessageService'
        });
    }

    obtenerListaTipoDocumento() {
        // this.utilServicio.showLoader();
        // this.apiServicio.obtenerTodos("tipoDocumento").subscribe(
        //     (result : any) => {
        //         console.log(result);
        //     },
        //     error => {
        //         console.log(error);
        //     }
        // );
        // this.apiServicio.obtenerTodos('tipoDocumento')
        //     //this.apiServicio.obtener()
        //     .then((result) => {
        //         if (this.utilServicio.dataDeServerEsCorrecta(result)) {
        //             console.log(result);
        //             // this.parametrosDocumento.tipoDocumentoId = this.tipoDocumentos[0].value;
        //         } else {
        //             this.messageDialog.showDialog(result.mensaje);
        //         }
        //         this.utilServicio.hideLoader();
        //     }).catch((error) => {
        //         this.utilServicio.hideLoader();
        //         this.messageDialog.showDialog('Error al conectar con el servidor');
        //     });
    }
}