import { Component, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'message-dialog',
    templateUrl: '../../plantilla/otro/message.dialog.html'
})
export class MessageDialog {

    private display?: boolean = false;
    private mensaje?: string;
    private titulo?: string;
    @Output() closeDialog = new EventEmitter();

    showDialog(mensaje: string, titulo: string = 'Sistema de almacén') {
        this.display = true;
        this.mensaje = mensaje;
        this.titulo = titulo;
    }

    onHide() {
        this.closeDialog.emit();
    }

    hideDialog() {
        this.display = false;
    }
}
