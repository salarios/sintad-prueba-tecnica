import { TipoDocumentoEntidad } from "./tipo.documento";
import { TipoContribuyenteEntidad } from "./tipo.contribuyente";

export class EntidadEntidad {

    id: number;
    tipoDocumento: TipoDocumentoEntidad;
    nroDocumento: String;
    razonSocial: String;
    nombreComercial?: String;
    tipoContribuyente: TipoContribuyenteEntidad;
    direccion?: String;
    telefono?: String;

    constructor() {
        this.tipoDocumento = new TipoDocumentoEntidad();
        this.tipoContribuyente = new TipoContribuyenteEntidad();
    }
}