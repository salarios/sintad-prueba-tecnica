"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RespuestaServicio = (function () {
    function RespuestaServicio(estado, mensaje, data) {
        if (mensaje === void 0) { mensaje = ""; }
        if (data === void 0) { data = null; }
        this.estado = estado;
        this.mensaje = mensaje;
        this.data = data;
    }
    return RespuestaServicio;
}());
exports.RespuestaServicio = RespuestaServicio;
