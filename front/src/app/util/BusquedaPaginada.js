"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BusquedaPaginada = (function () {
    function BusquedaPaginada() {
        this.totalRegistros = 1;
        this.totalPaginas = 1;
        this.cantidadPorPagina = 10;
        this.paginaActual = 1;
        this.buscar = {};
        this.registros = [];
    }
    return BusquedaPaginada;
}());
exports.BusquedaPaginada = BusquedaPaginada;