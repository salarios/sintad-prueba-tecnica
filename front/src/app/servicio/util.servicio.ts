import { Injectable, ElementRef } from '@angular/core';
// import 'rxjs/add/operator/toPromise';
import { RespuestaServicio } from '../util/respuesta.servicio';
import { Router } from '@angular/router';
import { BusquedaPaginada } from '../util/busqueda.paginada';

@Injectable()
export class UtilServicio {

    MONTH_NAMES: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    DAY_NAMES: string[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];


    nuevo(botones : any) {
        botones.nuevo = true;
        botones.guardar = false;
        botones.modificar = true;
        botones.eliminar = true;
        botones.cancelar = false;
    }

    cancelar(botones : any) {
        botones.nuevo = false;
        botones.guardar = true;
        botones.modificar = true;
        botones.eliminar = true;
        botones.cancelar = true;
    }

    modificar(botones : any) {
        botones.nuevo = true;
        botones.guardar = false;
        botones.modificar = true;
        botones.eliminar = true;
        botones.cancelar = false;
    }

    seleccionar(botones : any) {
        botones.nuevo = true;
        botones.guardar = true;
        botones.modificar = false;
        botones.eliminar = false;
        botones.cancelar = false;
    }

    setFocus(inputFocus: ElementRef) {
        setTimeout(() => {
            inputFocus.nativeElement.focus();
        }, 0);
    }

    selectInput(inputFocus: ElementRef) {
        setTimeout(() => {
            const inputElem = <HTMLInputElement>inputFocus.nativeElement;
            inputElem.select();
        }, 0);
    }

    getDateProperties() {
        return {
            firstDayOfWeek: 0,
            dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: 'Hoy',
            clear: 'Limpiar'
        };
    }

    formatBusquedaPaginadaParaEnviar(busquedaPaginada: BusquedaPaginada<any>, paginaActual: number): BusquedaPaginada<any> {
        return {
            paginaActual: paginaActual,
            buscar: busquedaPaginada.buscar,
            cantidadPorPagina: busquedaPaginada.cantidadPorPagina
        }
    }

    esNullOUndefined(valor: any): boolean {
        return typeof valor === "undefined" || valor == null;
    }

    esNullOUndefinedOVacio(valor: any): boolean {
        return this.esNullOUndefined(valor) || valor == "";
    }

    haSeleccionadoEntidad(entidad: any): boolean {
        if (this.esNullOUndefined(entidad)) {
            return false;
        }
        if (this.esNullOUndefinedOVacio(entidad.id)) {
            return false;
        }
        return true;
    }

    esArrayVacio(array: any): boolean {
        return this.esNullOUndefined(array) || array.length === 0;
    }

    dataDeServerEsCorrecta(data: RespuestaServicio): boolean {
        return !this.esNullOUndefined(data) && !this.esNullOUndefinedOVacio(data.estado) && data.estado === "exito";
    }

    showLoader() {
        let loader = document.getElementById("loader");
        if (loader != null) {
            loader.style.display = "block";
        }
    }

    hideLoader() {
        let loader = document.getElementById("loader");
        if (loader != null) {
            loader.style.display = "none";
        }
    }

    showNotificacion(mensaje: string) {
        this.hideNotificacion();
        let notificacionMsg = document.getElementById("notification-msg");
        let notificacion = document.getElementById("notification");
        if (notificacionMsg != null) {
            notificacionMsg.innerHTML = mensaje;
        }
        if (notificacion != null) {
            notificacion.style.display = "block";
        }
        setTimeout(() => {
            this.hideNotificacion();
        }, 10000);
    }

    hideNotificacion() {
        let notificacion = document.getElementById("notification");
        if (notificacion != null) {
            notificacion.style.display = "none";
        }
    }

    obtenerUsuarioLogeado(): any {
        let user = localStorage.getItem('usuario');
        // return (<any> Object).assign({}, JSON.parse(user));
        return user;
    }

    iniciarSesion(router: Router, result: RespuestaServicio) {
        // localStorage.setItem('usuario', JSON.stringify(result.data));
        localStorage.setItem('usuario', result.data);
        router.navigateByUrl('inicio');
    }

    cerrarSesion(router: Router) {
        localStorage.removeItem('usuario');
        router.navigateByUrl('logeo');
    }

    eliminarUsuarioSession() {
        localStorage.removeItem('usuario');
    }

    LZ(x : any) {
        return (x < 0 || x > 9 ? "" : "0") + x
    }
    
}