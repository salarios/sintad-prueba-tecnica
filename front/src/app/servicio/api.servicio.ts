import { Injectable, Inject } from '@angular/core';
// import { Response, Headers, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/toPromise';
import { BusquedaPaginada } from '../util/busqueda.paginada';
import { RespuestaServicio } from '../util/respuesta.servicio';
import { UtilServicio } from './util.servicio';
import { Config } from '../util/app.config';

@Injectable()
export class ApiServicio {
    private apiRoot: string = Config.API_URL;
    // private options: RequestOptions;
    // private optionsBin: RequestOptions;
    private headers: Headers;

    constructor( @Inject(HttpClient) private http: HttpClient, @Inject(UtilServicio) private utilServicio: UtilServicio) {
        this.headers = new Headers({
            'Content-Type': 'application/json;charset=UTF-8',
            'Accept': 'application/json, text/plain, */*',
            'x-auth-token': this.utilServicio.obtenerUsuarioLogeado().token
        });
    }

    setHeaders() {
        this.headers = new Headers({
            'Content-Type': 'application/json;charset=UTF-8',
            'Accept': 'application/json, text/plain, */*',
            'x-auth-token': this.utilServicio.obtenerUsuarioLogeado().token
        });
        // this.options = new RequestOptions({ headers: this.headers });
        // this.optionsBin = new RequestOptions({
        //     responseType: ResponseContentType.Blob,
        //     headers: this.headers
        // });
    }

    obtenerTodos(path: string) : Observable<any>{
        let peticion : string;
        peticion = this.apiRoot + path;
        return this.http.get(peticion);
    }
/*
    obtener(path: string, id: any): Promise<RespuestaServicio> {
        let promise = new Promise<RespuestaServicio>((resolve, reject) => {
            let apiURL = `${this.apiRoot}${path}/${id}`;
            this.http.get(apiURL, this.options)
                .toPromise()
                .then(res => { resolve(res.json() || {}) })
                .catch(error => { reject(this.getError(error)) });
        });
        return promise;
    }

    reporte(path: string, params: any) {
        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.apiRoot}${path}`;
            let body = JSON.stringify(params);
            this.http
                .post(apiURL, body, this.optionsBin)
                .toPromise()
                .then(res => { resolve(res.blob() || {}) })
                .catch(error => { reject(this.getError(error)) });

        });
        return promise;
    }


    // obtenerTodos(path: string): Promise<RespuestaServicio> {
    //     let promise = new Promise<RespuestaServicio>((resolve, reject) => {
    //         let apiURL = `${this.apiRoot}${path}`;
    //         this.http.get(apiURL, this.options)
    //             .toPromise()
    //             .then(res => { resolve(res.json() || {}) })
    //             .catch(error => { reject(this.getError(error)) });
    //     });
    //     return promise;
    // }

    eliminar(path: string, id: number): Promise<RespuestaServicio> {
        let promise = new Promise<RespuestaServicio>((resolve, reject) => {
            let apiURL = `${this.apiRoot}${path}/${id}`;
            this.http.delete(apiURL, this.options)
                .toPromise()
                .then(res => { resolve(res.json() || {}) })
                .catch(error => { reject(this.getError(error)) });
        });
        return promise;
    }

    guardar(path: string, data: any): Promise<RespuestaServicio> {
        let promise = new Promise<RespuestaServicio>((resolve, reject) => {
            let apiURL = `${this.apiRoot}${path}`;
            let body = JSON.stringify(data);
            if (data.id) {
                this.http
                    .put(apiURL, body, this.options)
                    .toPromise()
                    .then(res => { resolve(res.json() || {}) })
                    .catch(error => { reject(this.getError(error)) });
            } else {
                this.http
                    .post(apiURL, body, this.options)
                    .toPromise()
                    .then(res => { resolve(res.json() || {}) })
                    .catch(error => { reject(this.getError(error)) });
            }
        });
        return promise;
    }

    consultar(path: string, data: any) {
        let promise = new Promise<RespuestaServicio>((resolve, reject) => {
            let apiURL = `${this.apiRoot}${path}`;
            let body = JSON.stringify(data);
            this.http
                .post(apiURL, body, this.options)
                .toPromise()
                .then(res => { resolve(res.json() || {}) })
                .catch(error => { reject(this.getError(error)) });
        });
        return promise;
    }

    paginacion(path: string, busquedaPaginada: BusquedaPaginada<any>): Promise<RespuestaServicio> {
        let promise = new Promise<RespuestaServicio>((resolve, reject) => {
            let apiURL = `${this.apiRoot}${path}/paginacion`;
            let body = busquedaPaginada;
            this.http
                .post(apiURL, body, this.options)
                .toPromise()
                .then(res => { resolve(res.json() || {}) })
                .catch(error => { reject(this.getError(error)) });
        });
        return promise;
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    private getError(error: any) {
        return error.message || error;
    }
    */
}