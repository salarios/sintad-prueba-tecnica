CREATE SEQUENCE public.tb_tipo_contribuyente_id_seq;

CREATE TABLE public.tb_tipo_contribuyente (
                id INTEGER NOT NULL DEFAULT nextval('public.tb_tipo_contribuyente_id_seq'),
                nombre VARCHAR(50) NOT NULL,
                estado BOOLEAN NOT NULL DEFAULT TRUE,
                CONSTRAINT tb_tipo_contribuyente_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.tb_tipo_contribuyente_id_seq OWNED BY public.tb_tipo_contribuyente.id;

CREATE SEQUENCE public.tb_tipo_documento_id_seq;

CREATE TABLE public.tb_tipo_documento (
                id INTEGER NOT NULL DEFAULT nextval('public.tb_tipo_documento_id_seq'),
                codigo VARCHAR(20) NOT NULL,
                nombre VARCHAR(100) NOT NULL,
                descripcion VARCHAR(200),
                estado BOOLEAN NOT NULL DEFAULT TRUE,
                CONSTRAINT tb_tipo_documento_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.tb_tipo_documento_id_seq OWNED BY public.tb_tipo_documento.id;

CREATE SEQUENCE public.tb_entidad_id_seq;

CREATE TABLE public.tb_entidad (
                id INTEGER NOT NULL DEFAULT nextval('public.tb_entidad_id_seq'),
                id_tipo_documento INTEGER NOT NULL,
                nro_documento VARCHAR(25) NOT NULL,
                razon_social VARCHAR(100) NOT NULL,
                nombre_comercial VARCHAR(100),
                id_tipo_contribuyente INTEGER NOT NULL,
                direccion VARCHAR(250),
                telefono VARCHAR(50),
                estado BOOLEAN NOT NULL DEFAULT TRUE,
                CONSTRAINT tb_entidad_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.tb_entidad_id_seq OWNED BY public.tb_entidad.id;

ALTER TABLE public.tb_entidad ADD CONSTRAINT tb_tipo_contribuyente_tb_entidad_fk
FOREIGN KEY (id_tipo_contribuyente)
REFERENCES public.tb_tipo_contribuyente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.tb_entidad ADD CONSTRAINT tb_tipo_documento_tb_entidad_fk
FOREIGN KEY (id_tipo_documento)
REFERENCES public.tb_tipo_documento (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;


-- INSERCIÓN DE DATOS
INSERT INTO tb_tipo_documento VALUES ('1', '4', 'CARNET DE EXTRANJERIA', 'CARNET DE EXTRANJERIA', TRUE);
INSERT INTO tb_tipo_documento VALUES ('2', '7', 'PASAPORTE', 'PASAPORTE', TRUE);
INSERT INTO tb_tipo_documento VALUES ('3', '11', 'PARTIDA DE NACIMIENTO - IDENTIDAD', 'PARTIDA DE NACIMIENTO - IDENTIDAD', TRUE);
INSERT INTO tb_tipo_documento VALUES ('4', '99', 'OTROS', 'OTROS', TRUE);
INSERT INTO tb_tipo_documento VALUES ('5', '6', 'RUC', 'REGISTRO UNICO DEL CONTRIBUYENTE', TRUE);
INSERT INTO tb_tipo_documento VALUES ('6', '1', 'DNI', 'DOCUMENTO NACIONAL DE IDENTIDAD', TRUE);


INSERT INTO tb_tipo_contribuyente VALUES ('1', 'Natural Sin Negocio', TRUE);
INSERT INTO tb_tipo_contribuyente VALUES ('2', 'Juridica', TRUE);
INSERT INTO tb_tipo_contribuyente VALUES ('3', 'Natural Con Negocio', TRUE);
INSERT INTO tb_tipo_contribuyente VALUES ('4', 'No Domiciliado', TRUE);


INSERT INTO tb_entidad VALUES ('1', '3', '20505327552', 'SYL S.A.C', 'SYL CARGO NOMBRE COMERCIAL', '1', 'Jr. Comandante Jimenez Nro. 166 Int. a (entre Cuadra 7 y 8 Javier Padro Oeste)', '79845612', TRUE);
INSERT INTO tb_entidad VALUES ('2', '3', '20543844838', 'PUNTUAL EXPRESS S.A.C.', '', '1', 'MZA. F LOTE. 29 AS.RSD.MONTECARLO II LIMA - LIMA - SAN MARTIN DE PORRE', '', TRUE);
INSERT INTO tb_entidad VALUES ('3', '3', '10410192999', 'ALVAREZ MACHUCA RENZO GUSTAVO', '', '3', 'AV. LOS ALISOS MZA. G LOTE. 05 ASC. LA ALBORADA DE OQUENDO III ETAPA (CRUCE PTE OQUENDO CON AV.NESTOR GAMBETTA) PROV. CONST. DEL CALLAO - PROV. CONST. DEL CALLAO - CALLAO', '', TRUE);
INSERT INTO tb_entidad VALUES ('4', '3', '20600131037', 'CARNICOS MAFER S.A.C.', '', '2', 'CAL.EL UNIVERSO NRO. 327 URB. LA CAMPIÑA ZONA CINCO (ALTURA ', '', TRUE);
INSERT INTO tb_entidad VALUES ('5', '3', '20556528218', 'SUMAQUINARIA S.A.C.', '', '2', 'AV. M.SUCRE NRO. 455 DPTO. 603 LIMA - LIMA - MAGDALENA DEL MAR', '', TRUE);
INSERT INTO tb_entidad VALUES ('6', '3', '20545412528', 'OASIS FOODS S.A.C.', '', '2', 'CAL. FRANCISCO MASIAS NRO. 370 URB. SAN EUGENIO (PISO 7) LIM', '', TRUE);
INSERT INTO tb_entidad VALUES ('7', '3', '20510620195', 'INVERSIONES PRO3 SAC', '', '2', 'AV. AUTOPIDTA RAMIRO PRIALE LOTE. 02 A.V. PROP HUERTOS DE HU', '', TRUE);
INSERT INTO tb_entidad VALUES ('8', '3', '20498383361', 'REPUESTOS DAVID DIESEL E.I.R.L.', '', '2', 'CAR.VIA EVITAMIENTO MZA. 857 LOTE. 7 SEC. IRRIGACION EL CURAL 1 AREQUIPA - AREQUIPA - CERRO COLORADO', '', TRUE);
INSERT INTO tb_entidad VALUES ('9', '6', 'CNAH00003', 'ANHUI HAYVO PROTECTIVE PRODUCT MANUFACTURING CO.,LTD', '', '4', '173 FENGLE AVENUE,ECNOMIC DEVELOPMENT ZONE,QUANJIAO COUNTY', '', TRUE);