/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sintad.pruebatecnica.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

/**
 *
 * @author Marcos
 */
public class UsuarioSession implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String token;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String login;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String ip;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String pc;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long sucursalId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sucursal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nombre;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String apellidos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String serie;

    public UsuarioSession() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public Long getSucursalId() {
        return sucursalId;
    }

    public void setSucursalId(Long sucursalId) {
        this.sucursalId = sucursalId;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public static UsuarioSession clonar(UsuarioSession usuarioS) {
        UsuarioSession usuarioSession = new UsuarioSession();
        usuarioSession.setId(usuarioS.getId());
        usuarioSession.setLogin(usuarioS.getLogin());
        usuarioSession.setSerie(usuarioS.getSerie());
        usuarioSession.setSucursal(usuarioS.getSucursal());
        usuarioSession.setSucursalId(usuarioS.getSucursalId());
        usuarioSession.setApellidos(usuarioS.getApellidos());
        usuarioSession.setNombre(usuarioS.getNombre());
        usuarioSession.setPc(usuarioS.getPc());
        return usuarioSession;
    }

}
