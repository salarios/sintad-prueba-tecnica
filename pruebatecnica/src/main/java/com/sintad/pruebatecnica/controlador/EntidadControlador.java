package com.sintad.pruebatecnica.controlador;

import com.sintad.pruebatecnica.entidad.Entidad;
import com.sintad.pruebatecnica.enums.NombreEntidad;
import com.sintad.pruebatecnica.servicio.EntidadServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Marcos
 */
@RestController
@RequestMapping("/server/entidad")
public class EntidadControlador extends BaseControladorImpl<Entidad, Long> implements BaseControlador<Entidad, Long> {

    @Autowired
    public EntidadControlador(EntidadServicio entidadServicio) {
        super(entidadServicio, NombreEntidad.ENTIDAD.getValor());
    }

}