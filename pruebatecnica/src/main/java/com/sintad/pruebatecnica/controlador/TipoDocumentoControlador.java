package com.sintad.pruebatecnica.controlador;

import com.sintad.pruebatecnica.entidad.TipoDocumento;
import com.sintad.pruebatecnica.enums.NombreEntidad;
import com.sintad.pruebatecnica.servicio.TipoDocumentoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Marcos
 */
@RestController
@RequestMapping("/server/tipoDocumento")
public class TipoDocumentoControlador extends BaseControladorImpl<TipoDocumento, Long> implements BaseControlador<TipoDocumento, Long> {

    @Autowired
    public TipoDocumentoControlador(TipoDocumentoServicio tipoDocumentoServicio) {
        super(tipoDocumentoServicio, NombreEntidad.TIPO_DOCUMENTO.getValor());
    }

}
