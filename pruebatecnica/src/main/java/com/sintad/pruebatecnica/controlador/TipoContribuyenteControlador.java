package com.sintad.pruebatecnica.controlador;

import com.sintad.pruebatecnica.entidad.TipoContribuyente;
import com.sintad.pruebatecnica.enums.NombreEntidad;
import com.sintad.pruebatecnica.servicio.TipoContribuyenteServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Marcos
 */
@RestController
@RequestMapping("/server/tipoContribuyente")
public class TipoContribuyenteControlador extends BaseControladorImpl<TipoContribuyente, Long> implements BaseControlador<TipoContribuyente, Long> {

    @Autowired
    public TipoContribuyenteControlador(TipoContribuyenteServicio tipoContribuyenteServicio) {
        super(tipoContribuyenteServicio, NombreEntidad.TIPO_CONTRIBUYENTE.getValor());
    }

}
