package com.sintad.pruebatecnica.enums;

/**
 *
 * @author Marcos
 */
public enum NombreEntidad {
    
    ENTIDAD("Boleto"),
    TIPO_CONTRIBUYENTE("Tipo de contribuyente"),
    TIPO_DOCUMENTO("Tipo de documento");
    
    private String valor;

    private NombreEntidad(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
