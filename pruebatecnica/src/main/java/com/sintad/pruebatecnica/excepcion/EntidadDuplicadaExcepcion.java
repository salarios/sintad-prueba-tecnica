package com.sintad.pruebatecnica.excepcion;

/**
 *
 * @author Marcos
 */
public class EntidadDuplicadaExcepcion extends Exception {

    public EntidadDuplicadaExcepcion(String mensaje) {
        super(mensaje);
    }
}
