package com.sintad.pruebatecnica.repositorio.impl;

import com.sintad.pruebatecnica.entidad.TipoContribuyente;
import com.sintad.pruebatecnica.repositorio.TipoContribuyenteRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marcos
 */
@Repository
public class TipoContribuyenteRepositorioImpl extends BaseRepositorioImpl<TipoContribuyente, Long> implements TipoContribuyenteRepositorio {

}