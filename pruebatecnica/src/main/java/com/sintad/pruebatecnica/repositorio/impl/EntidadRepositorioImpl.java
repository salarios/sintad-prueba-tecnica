package com.sintad.pruebatecnica.repositorio.impl;

import com.sintad.pruebatecnica.entidad.Entidad;
import com.sintad.pruebatecnica.repositorio.EntidadRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marcos
 */
@Repository
public class EntidadRepositorioImpl extends BaseRepositorioImpl<Entidad, Long> implements EntidadRepositorio {

}
