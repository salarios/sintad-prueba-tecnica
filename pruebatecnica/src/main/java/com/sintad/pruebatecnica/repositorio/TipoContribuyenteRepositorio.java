package com.sintad.pruebatecnica.repositorio;

import com.sintad.pruebatecnica.entidad.TipoContribuyente;

/**
 *
 * @author Marcos
 */
public interface TipoContribuyenteRepositorio extends BaseRepositorio<TipoContribuyente, Long> {
    
}