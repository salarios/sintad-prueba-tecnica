package com.sintad.pruebatecnica.repositorio;

import com.sintad.pruebatecnica.entidad.Entidad;

/**
 *
 * @author Marcos
 */
public interface EntidadRepositorio extends BaseRepositorio<Entidad, Long> {
    
}
