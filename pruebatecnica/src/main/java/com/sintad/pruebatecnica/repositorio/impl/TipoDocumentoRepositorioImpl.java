package com.sintad.pruebatecnica.repositorio.impl;

import com.sintad.pruebatecnica.entidad.TipoDocumento;
import com.sintad.pruebatecnica.repositorio.TipoDocumentoRepositorio;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marcos
 */
@Repository
public class TipoDocumentoRepositorioImpl extends BaseRepositorioImpl<TipoDocumento, Long> implements TipoDocumentoRepositorio {

}
