package com.sintad.pruebatecnica.repositorio;

import com.sintad.pruebatecnica.entidad.TipoDocumento;

/**
 *
 * @author Marcos
 */
public interface TipoDocumentoRepositorio extends BaseRepositorio<TipoDocumento, Long> {
    
}
