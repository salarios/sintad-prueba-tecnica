package com.sintad.pruebatecnica.servicio.impl;

import com.sintad.pruebatecnica.entidad.TipoContribuyente;
import com.sintad.pruebatecnica.enums.NombreEntidad;
import com.sintad.pruebatecnica.excepcion.EntidadDuplicadaExcepcion;
import com.sintad.pruebatecnica.repositorio.TipoContribuyenteRepositorio;
import com.sintad.pruebatecnica.servicio.TipoContribuyenteServicio;
import com.sintad.pruebatecnica.util.RespuestaControlador;
import com.sintad.pruebatecnica.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Marcos
 */
@Service
public class TipoContribuyenteServicioImpl extends BaseServicioImpl<TipoContribuyente, Long> implements TipoContribuyenteServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TipoContribuyenteRepositorio tipoContribuyenteRepositorio;

    @Autowired
    public TipoContribuyenteServicioImpl(TipoContribuyenteRepositorio tipoContribuyenteRepositorio) {
        super(tipoContribuyenteRepositorio);
    }

    @Override
    public RespuestaControlador crear(TipoContribuyente tipoContribuyente) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoContribuyente);
        this.tipoContribuyenteRepositorio.crear(tipoContribuyente);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TIPO_CONTRIBUYENTE.getValor(), tipoContribuyente.getId());
    }

    @Override
    public RespuestaControlador actualizar(TipoContribuyente tipoContribuyente) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoContribuyente);
        this.tipoContribuyenteRepositorio.actualizar(tipoContribuyente);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TIPO_CONTRIBUYENTE.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long contribuyenteId) {
        RespuestaControlador respuesta;
        TipoContribuyente tipoContribuyente;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TIPO_CONTRIBUYENTE.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            tipoContribuyente = tipoContribuyenteRepositorio.obtener(contribuyenteId);
            tipoContribuyente.setEstado(Boolean.FALSE);
            tipoContribuyenteRepositorio.actualizar(tipoContribuyente);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TIPO_CONTRIBUYENTE.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TipoContribuyente tipoContribuyente) throws EntidadDuplicadaExcepcion {
//        Criterio filtro = Criterio.forClass(TipoContribuyente.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("nombre", tipoContribuyente.getNombre()));
//
//        // Si es una actualizacion
//        if (tipoContribuyente.getId() != null) {
//            filtro.add(Restrictions.ne("id", tipoContribuyente.getId()));
//        }
//        if (tipoContribuyenteRepositorio.cantidadPorCriteria(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya se ha registrado un " + NombreEntidad.TIPO_CONTRIBUYENTE.getValor() + " anteriormente.");
//        }
    }

}

