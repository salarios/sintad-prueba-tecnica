package com.sintad.pruebatecnica.servicio.impl;

import com.sintad.pruebatecnica.entidad.TipoDocumento;
import com.sintad.pruebatecnica.enums.NombreEntidad;
import com.sintad.pruebatecnica.excepcion.EntidadDuplicadaExcepcion;
import com.sintad.pruebatecnica.repositorio.TipoDocumentoRepositorio;
import com.sintad.pruebatecnica.servicio.TipoDocumentoServicio;
import com.sintad.pruebatecnica.util.RespuestaControlador;
import com.sintad.pruebatecnica.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Marcos
 */
@Service
public class TipoDocumentoServicioImpl extends BaseServicioImpl<TipoDocumento, Long> implements TipoDocumentoServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private TipoDocumentoRepositorio tipoDocumentoRepositorio;

    @Autowired
    public TipoDocumentoServicioImpl(TipoDocumentoRepositorio tipoDocumentoRepositorio) {
        super(tipoDocumentoRepositorio);
    }

    @Override
    public RespuestaControlador crear(TipoDocumento tipoDocumento) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoDocumento);
        this.tipoDocumentoRepositorio.crear(tipoDocumento);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.TIPO_DOCUMENTO.getValor(), tipoDocumento.getId());
    }

    @Override
    public RespuestaControlador actualizar(TipoDocumento tipoDocumento) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(tipoDocumento);
        this.tipoDocumentoRepositorio.actualizar(tipoDocumento);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.TIPO_DOCUMENTO.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long tipoDocumentoId) {
        RespuestaControlador respuesta;
        TipoDocumento tipoDocumento;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("El " + NombreEntidad.TIPO_DOCUMENTO.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            tipoDocumento = tipoDocumentoRepositorio.obtener(tipoDocumentoId);
            tipoDocumento.setEstado(Boolean.FALSE);
            tipoDocumentoRepositorio.actualizar(tipoDocumento);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.TIPO_DOCUMENTO.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(TipoDocumento tipoDocumento) throws EntidadDuplicadaExcepcion {
//        Criterio filtro = Criterio.forClass(TipoDocumento.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("nombre", tipoDocumento.getNombre()));
//
//        // Si es una actualizacion
//        if (tipoDocumento.getId() != null) {
//            filtro.add(Restrictions.ne("id", tipoDocumento.getId()));
//        }
//        if (tipoDocumentoRepositorio.cantidadPorCriteria(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya se ha registrado un " + NombreEntidad.TIPO_DOCUMENTO.getValor() + " anteriormente.");
//        }
    }

}
