package com.sintad.pruebatecnica.servicio;

import com.sintad.pruebatecnica.entidad.TipoDocumento;

/**
 *
 * @author Marcos
 */
public interface TipoDocumentoServicio extends BaseServicio<TipoDocumento, Long> {

}
