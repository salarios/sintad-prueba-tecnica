package com.sintad.pruebatecnica.servicio;

import com.sintad.pruebatecnica.entidad.Entidad;

/**
 *
 * @author Marcos
 */
public interface EntidadServicio extends BaseServicio<Entidad, Long> {

}
