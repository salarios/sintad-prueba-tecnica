package com.sintad.pruebatecnica.servicio.impl;

import com.sintad.pruebatecnica.entidad.Entidad;
import com.sintad.pruebatecnica.enums.NombreEntidad;
import com.sintad.pruebatecnica.excepcion.EntidadDuplicadaExcepcion;
import com.sintad.pruebatecnica.repositorio.EntidadRepositorio;
import com.sintad.pruebatecnica.servicio.EntidadServicio;
import com.sintad.pruebatecnica.util.RespuestaControlador;
import com.sintad.pruebatecnica.util.RespuestaControladorServicio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Marcos
 */
@Service
public class EntidadServicioImpl extends BaseServicioImpl<Entidad, Long> implements EntidadServicio {

    private final Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private RespuestaControladorServicio respuestaControladorServicio;

    @Autowired
    private EntidadRepositorio entidadRepositorio;

    @Autowired
    public EntidadServicioImpl(EntidadRepositorio entidadRepositorio) {
        super(entidadRepositorio);
    }

    @Override
    public RespuestaControlador crear(Entidad entidad) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(entidad);
        this.entidadRepositorio.crear(entidad);
        return this.respuestaControladorServicio.obtenerRespuestaDeExitoCrearConData(NombreEntidad.ENTIDAD.getValor(), entidad.getId());
    }

    @Override
    public RespuestaControlador actualizar(Entidad entidad) throws EntidadDuplicadaExcepcion {
        this.validarDuplicado(entidad);
        this.entidadRepositorio.actualizar(entidad);
        return respuestaControladorServicio.obtenerRespuestaDeExitoActualizar(NombreEntidad.ENTIDAD.getValor());
    }

    @Override
    public RespuestaControlador eliminar(Long entidadId) {
        RespuestaControlador respuesta;
        Entidad entidad;
        Boolean puedeEliminar;

        puedeEliminar = true;

        if (puedeEliminar == null || !puedeEliminar) {
            respuesta = RespuestaControlador.obtenerRespuestaDeError("La " + NombreEntidad.ENTIDAD.getValor().toLowerCase() + " ha sido asignado a uno o varios usuarios y no se puede eliminar");
        } else {
            entidad = entidadRepositorio.obtener(entidadId);
            entidad.setEstado(Boolean.FALSE);
            entidadRepositorio.actualizar(entidad);
            respuesta = respuestaControladorServicio.obtenerRespuestaDeExitoEliminar(NombreEntidad.ENTIDAD.getValor());
        }

        return respuesta;
    }

    public void validarDuplicado(Entidad entidad) throws EntidadDuplicadaExcepcion {
//        Criterio filtro = Criterio.forClass(Entidad.class);
//
//        filtro.add(Restrictions.eq("estado", Boolean.TRUE));
//        filtro.add(Restrictions.eq("nombreComercial", entidad.getNombreComercial()));
//
//        // Si es una actualizacion
//        if (entidad.getId() != null) {
//            filtro.add(Restrictions.ne("id", entidad.getId()));
//        }
//        if (entidadRepositorio.cantidadPorCriteria(filtro) > 0) {
//            throw new EntidadDuplicadaExcepcion("Ya se ha registrado un " + NombreEntidad.ENTIDAD.getValor() + " anteriormente.");
//        }
    }

}
