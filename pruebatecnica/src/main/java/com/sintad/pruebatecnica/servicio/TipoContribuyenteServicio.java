package com.sintad.pruebatecnica.servicio;

import com.sintad.pruebatecnica.entidad.TipoContribuyente;

/**
 *
 * @author Marcos
 */
public interface TipoContribuyenteServicio extends BaseServicio<TipoContribuyente, Long> {

}
